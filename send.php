<!DOCTYPE html>
<html lang='ru'>
<?php session_start();?>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <title>First Test PHP App!</title>
    <!-- Connect bootstrap and personal styles -->
    <link href='styles/style.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
</head>

<body class='d-flex align-items-center justify-content-center' style='height: 100vh'>
<?php
// Declare function that checks if any input left empty by user
function isEmpty($arr): bool
{
    $error = false;
    foreach ($arr as $key => $val) {
        if (empty($val)) {
            echo "error: input $key is empty!\n"; //Debug line
            $error = true;
        }
    }
    return $error;
}

// Get trimmed inputs into associative array
$allInputs = [
            "fName" => trim($_POST["fName"]),
            "lName" => trim($_POST["lName"]),
            "email" => trim($_POST["email"]),
            "pass" => trim($_POST["pass"])
            ];

// Checking if fields are empty or corrupt, returns to register page if any is empty or corrupt
if (isEmpty($allInputs)) {
    $_SESSION["reg_error"] = "Please do not leave any fields empty!";
    foreach($allInputs as $field => $val) {
        if ($field === "pass") {continue;}
            switch ($field) {
                case "email":
                    $val = filter_var($val, FILTER_SANITIZE_EMAIL);
                    break;
                default:
                    $val = filter_var($val, FILTER_SANITIZE_STRING);
            }
        if ($val) {
            $_SESSION[$field] = $val;
        }
    }
//    sleep(3); //Debug line
    header("Location:/?decision=Register");
    exit();
}

//Length and format validation to be implemented
//...

//Hashing password with "salt"
$allInputs["pass"] = md5($allInputs["pass"] . "salt");

// Open connection to database
require "db_config.php";

// Creating query string
$fName = $allInputs["fName"];
$lName = $allInputs["lName"];
$email = $allInputs["email"];
$pass = $allInputs["pass"];
$query = "INSERT INTO users (f_name, l_name, email, password) VALUES ('$fName', '$lName', '$email', '$pass')";

// Record and return if record was successful
if ($mysql->query($query) === true) {
    echo "<div class='d-flex flex-column justify-content-center align-items-center talc'>
            <h1>Thanks for your submission!</h1>
            <h2>Registration went successfully ;-)</h2>
            <form action='index.php' method='get' class='mt-1'>
                <input type='submit' name='decision' value='Login' class='btn btn-secondary'>
            </form>
        </div>";
} else {
    // If query was unsuccessful
    echo "<h3>ERROR: Could not able to execute $query. " . $mysql->error . "</h3>";
}

// Destroy session and sql connection if user is logged
session_destroy();
mysqli_close($mysql);

//# debug lines: 2
?>
</body>