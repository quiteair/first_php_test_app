<?php
// Check if mysqli exist (commented because it exists)
if (!function_exists('mysqli_init') && !extension_loaded('mysqli')) {
    echo 'We don\'t have mysqli!!!';
}

// Database configuration
$dbHost     = "localhost";
$dbUsername = "first_hero_api";
$dbPassword = "admin";
$dbName     = "first_project_hero";

// Open connection
$mysql = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

// Check connection
if ($mysql -> connect_errno) {
    echo "Failed to connect to MySQL: " . $mysql -> connect_error;
    exit();
}