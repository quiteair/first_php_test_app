<?php
session_start();

// Fetch results from FORM submission
$email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
$pass = md5($_POST["pass"] . "salt");

// Open connection to database
require "db_config.php";

// Create and call the query
$query = "SELECT * FROM `users` WHERE `email` = '$email' AND `password` = '$pass'";
$result = $mysql -> query($query);

$user = $result -> fetch_assoc();

// Check if user not exist
if (count($user) == 0) {
    // Return to login page
    $_SESSION["log_error"] = "Password or email is incorrect";
    $_SESSION["email"] = $email;
    header("Location:/?decision=Login");
    exit();
}

// Destroy session and sql connection if user is logged
session_destroy();
mysqli_close($mysql);

// Set cookie that user is logged in
setcookie("logged", $user["id"], time() + 10800, "/");

// Stop the script
header("Location:/");
exit();