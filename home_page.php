<!DOCTYPE html>
<html lang='ru'>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <title>First Test PHP App!</title>
    <!-- Connect bootstrap and personal styles -->
    <link href='styles/style.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
</head>

<?php
// Open connection
require "db_config.php";

// Get user info from DB
$id = $_COOKIE["logged"];
$query = "SELECT * FROM `users` WHERE `id` = '$id'";
$result = $mysql -> query($query);
$user = $result -> fetch_assoc();

// Check if image exist for user
$query = "SELECT * FROM `profile_images` WHERE `user_id` = '$id'";
$result = $mysql -> query($query);
$user_image = $result -> fetch_assoc();

function renderImageSubmit() {
    echo "
            <form action='upload.php' method='post' enctype='multipart/form-data' class='talc mb-4'>
                <div class='mb-3'>
                    <label for='inputFile' class='form-label'><b>Please Choose Profile Image</b></label>
                    <input type='file' name='file' accept='.png,.jpg,.jpeg' id='inputFile' class='form-control'>
                </div>
                <input type='submit' name='submit' value='Upload' class='btn btn-secondary login-btn'>
            </form>";
}
function renderImage($user_image) {
    $url = $user_image["file"];
    echo "<img src='$url' class='card-img-top' alt='profile_image'>";
}
?>

<body>
    <div class='main-div form-width d-flex flex-column align-items-center justify-content-center' style='height: 100vh'>
        <h1>You are logged in</h1>
        <h2>User info:</h2>
        <div class='card form-width mb-4'>
            <div class="card-body d-flex flex-column align-items-center justify-content-start">
            <?php
                if (!$user_image) {
                    renderImageSubmit();
                } else {
                    renderImage($user_image);
                }

                foreach ($user as $field => $val) {
                    if ($field == "password" or $field == "id") {continue;}
                    echo "<div class='d-flex justify-content-between user-info'>
                            <span class='badge bg-secondary'>" . $field . "</span> <p>" . $val . "</p>
                        </div>";

                }
            ?>
            </div>
        </div>
        <p>Click <a href="/exit.php">here</a> to log out</p>
    </div>
</body>

<?php
    mysqli_close($mysql);
?>