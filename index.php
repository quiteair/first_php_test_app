<!DOCTYPE html>
<html lang='ru'>
<?php session_start();?>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <title>First Test PHP App!</title>
    <!-- Connect bootstrap and personal styles -->
    <link href='styles/style.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
</head>

<?php
    // Define rendering function
    function registerForm() {
        echo "<div class='card'>
                <form action='send.php' method='post' class='card-body'>
                    <div class='mb-3'>
                        <label for='inputFN' class='form-label'>First name</label>
                        <input type='text' name='fName' value='" . $_SESSION["fName"] . "' id='inputFN' class='form-control'>
                    </div>
                    <div class='mb-3'>
                        <label for='inputLN' class='form-label'>Last name</label>
                        <input type='text' name='lName' value='" . $_SESSION["lName"] . "' id='inputLN' class='form-control'>
                    </div>
                    <div class='mb-3'>
                        <label for='inputEmail' class='form-label'>Email address</label>
                        <input type='email' name='email' value='" . $_SESSION["email"] . "' id='inputEmail' class='form-control' aria-describedby='emailHelp'>
                        <div id='emailHelp' class='form-text'>We'll never share your email with anyone else.</div>
                    </div>
                    <div class='mb-3'>
                        <label for='inputPassword' class='form-label'>Password</label>
                        <input type='password' name='pass' id='inputPassword' class='form-control'>
                    </div>
                    <div>
                        <input type='submit' name='submit' value='Register'  class='btn btn-secondary register-btn' aria-describedby='submitHelp'>";
                        if (isset($_SESSION['reg_error'])) {
                            echo "<div id='submitHelp' class='error form-text'>" . $_SESSION['reg_error'] . "</div>";
                        }
                echo "</div>
                </form>
            </div>";
    }
    function loginForm() {
        echo "<div class='card form-width'>
                <form action='auth.php' method='post' class='card-body'>
                    <div class='mb-3'>
                        <label for='inputEmail' class='form-label'>Email address</label>
                        <input type='email' name='email' value='" . $_SESSION["email"] . "' id='inputEmail' class='form-control'>
                    </div>
                    <div class='mb-3'>
                        <label for='inputPassword' class='form-label'>Password</label>
                        <input type='password' name='pass' id='inputPassword' class='form-control'>
                    </div>
                        <input type='submit' name='submit' value='Sign in'  class='btn btn-secondary login-btn'>";
                        if (isset($_SESSION['log_error'])) {
                            echo "<div id='submitHelp' class='error form-text'>" . $_SESSION['log_error'] . "</div>";
                        }
                echo "</form>
            </div>";
    }
    function showForm($case) {
        switch ($case) {
            case "Lending":
                echo "<form action='' method='get'>
                        <input type='submit' name='decision' value='Login' class='btn btn-light'>
                        <input type='submit' name='decision' value='Register' class='btn btn-light'>
                    </form>";
                break;
            case "Register":
                registerForm();
                break;
            case "Login":
                loginForm();
                break;
        }
    }
?>

<body class='d-flex align-items-center justify-content-center' style='height: 100vh'>
<?
    // Check if user is logged in
    if (isset($_COOKIE["logged"])) {
        header("Location:/home_page.php");
        exit();
    }
?>
    <div class='d-flex flex-column justify-content-center align-items-center' style='height: 80vh'>
        <header>
            <h1 id='t1'>Welcome!</h1>
            <?php
            $welcomeMessage = "Lending";
            if ($_SERVER["REQUEST_METHOD"] === "GET") {
                switch ($_GET["decision"]) {
                    case "Login":
                        $welcomeMessage = "Login";
                        break;
                    case "Register":
                        $welcomeMessage = "Register";
                        break;
                }
            }
            ?>
            <h2 id='t2'>To <?=$welcomeMessage;?>
                Page ;-)</h2>
        </header>

        <div class="mt-4">
            <?php
                if (!isset($_GET["decision"])) {
                    showForm("Lending");
                    exit();
                }
                $decision = $_GET["decision"];
                switch ($decision) {
                    case "Login":
                    case "Register":
                        showForm($decision);
                        break;
                    default:
                        showForm("Lending");
                }
            ?>
        </div>
    </div>
</body>

</html>