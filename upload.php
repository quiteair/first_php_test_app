<?php
// Open DB connection
require "db_config.php";

// Check if file was uploaded correctly
if(!isset($_FILES["file"]) || $_FILES["file"]["error"] > 0){
    echo "Error did not upload properly";
    mysqli_close($mysql);
    exit();
}

// Verify file extension
$filetype = $_FILES["file"]["type"];
$allowed = ["jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png"];
if (!in_array($filetype, $allowed)) {
    echo "File is wrong format, only jpg and png allowed";
    mysqli_close($mysql);
    exit();
}

// Creating subdirectory path to have no problems if images have same name
$id = $_COOKIE["logged"];
$subDir = "$id";
mkdir("./uploads/$subDir", 0755);
$targetDir = "uploads/$subDir/";
// Creating final dir path
$finalTargetDir = $targetDir . basename($_FILES["file"]["name"]);
echo $finalTargetDir;
if (move_uploaded_file($_FILES["file"]["tmp_name"], $finalTargetDir)) {
    $id = (int)$id;
    $insert = $mysql->query("INSERT INTO profile_images (user_id, file) VALUES ('$id', '$finalTargetDir')");
    mysqli_close($mysql);
    header("Location:/");
    exit();
}
echo "Error";
// Close DB connection
mysqli_close($mysql);